﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CarRentalAgency.Startup))]
namespace CarRentalAgency
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
