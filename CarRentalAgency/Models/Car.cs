﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarRentalAgency.Models
{
    public class Car
    {
        [Key]
        public int CarId { get; set; }
        [StringLength(100)]
        [Required]
        public string Name { get; set; }
        [Required]
        public int Seats { get; set; }
        [Required]
        [Display(Name = "large bags number")]
        public int LargeBagNum { get; set; }
        [Display(Name = "Small bags number")]
        public int SmallBagNum { get; set; }
        [Required]
        public Size SizeId { get; set; }
        [Required]
        public double Rated { get; set; }
        public byte[] Photo { get; set; }
        [Required]
        [Display(Name = "Price/day")]
        public int PricePerDay { get; set; }
        [Required]
        [Display(Name = "Air Conditioning")]
        public Boolean AirConditioning { get; set; }
        [Required]
        [Display(Name = "Automatic Transmission")]
        public Boolean AutomaticTransmission { get; set; }
        public Boolean Available { get; set; }
        public enum Size
        {
            Compact,
            Economy,
            FullSize,
            Intermediate,
            Luxury,
            Premium,
            Special,
            Standard
        }
    }
}