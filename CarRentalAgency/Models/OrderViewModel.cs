﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarRentalAgency.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Rental Location")]
        public string Location { get; set; }
        [Display(Name = "Drop off")]
        public string Destination { get; set; }
        [Required]
        [Display(Name = "Pick-up Date:")]
        public DateTime StarTime { get; set; }
        [Required]
        [Display(Name = "Drop off Date:")]
        public DateTime EndTime { get; set; }
        public DateTime DealTime { get; set; }
        [Display(Name = "Total Price")]
        public int TotalPrice { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int CarId { get; set; }
        public Car Car { get; set; }
    }
}