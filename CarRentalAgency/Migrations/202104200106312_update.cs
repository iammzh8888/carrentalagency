namespace CarRentalAgency.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "UserId", c => c.String());
            AlterColumn("dbo.Orders", "UserName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "UserName", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "UserId", c => c.String(nullable: false));
        }
    }
}
