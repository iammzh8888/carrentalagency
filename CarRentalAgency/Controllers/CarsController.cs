﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarRentalAgency.Helper;
using CarRentalAgency.Models;
using PagedList;

namespace CarRentalAgency.Controllers
{
    public class CarsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Cars
        public ActionResult Index(string sortDir, string searchInput, string currentFilter, int? page, string sortOrder = "")
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var cars = db.Cars.AsQueryable();
            cars = cars.OrderBy(c => c.PricePerDay);
            if (!User.IsInRole(RoleName.CanManage))
            {
                cars = cars.Where(c => c.Available == true);
            }
            int pageSize = 5;
            int pageNumber = page ?? 1;
            string para = Request.QueryString["page"];
            if (string.IsNullOrEmpty(para) && string.IsNullOrEmpty(searchInput) && string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(sortDir))
            {
                var data1 = cars.ToPagedList(pageNumber, pageSize);

                //is checking the loginned user
                if (User.IsInRole(RoleName.CanManage))
                    return this.View(data1);
                else
                    return this.View("ReadOnly", data1);
            }

            if (!string.IsNullOrEmpty(searchInput))
                cars = cars.Where(c => c.Name.ToLower().Contains(searchInput.ToLower()));

            switch (sortOrder.ToLower())
            {
                case "name":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.Name);
                    else
                        cars = cars.OrderBy(c => c.Name);
                    break;
                case "seats":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.Seats);
                    else
                        cars = cars.OrderBy(c => c.Seats);
                    break;
                case "largebagnum":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.LargeBagNum);
                    else
                        cars = cars.OrderBy(c => c.LargeBagNum);
                    break;
                case "smallbagnum":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.SmallBagNum);
                    else
                        cars = cars.OrderBy(c => c.SmallBagNum);
                    break;
                case "sizeid":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.SizeId);
                    else
                        cars = cars.OrderBy(c => c.SizeId);
                    break;
                case "rated":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.Rated);
                    else
                        cars = cars.OrderBy(c => c.Rated);
                    break;
                case "priceperday":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.PricePerDay);
                    else
                        cars = cars.OrderBy(c => c.PricePerDay);
                    break;
                case "airconditioning":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.AirConditioning);
                    else
                        cars = cars.OrderBy(c => c.AirConditioning);
                    break;
                case "automatictransmission":
                    if (sortDir.ToLower() == "desc")
                        cars = cars.OrderByDescending(c => c.AutomaticTransmission);
                    else
                        cars = cars.OrderBy(c => c.AutomaticTransmission);
                    break;
                default:
                    cars = cars.OrderBy(c => c.PricePerDay);
                    break;
            }

            var data = cars.ToPagedList(pageNumber, pageSize);

            if (User.IsInRole(RoleName.CanManage))
                return this.PartialView("_CarView", data);
            else
                return this.PartialView("_CarViewReadOnly", data);
        }

        public ActionResult Search(string sortDir, string searchInput, string currentFilter, int? page, OrderViewModel orderVm, string sortOrder = "")
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var result = db.Cars.AsQueryable();
            result = result.OrderBy(c => c.PricePerDay);
            if (!User.IsInRole(RoleName.CanManage))
            {
                result = result.Where(c => c.Available == true);
            }
            if (!string.IsNullOrEmpty(searchInput))
                result = result.Where(c => c.Name.ToLower().Contains(searchInput.ToLower()));

            int pageSize = 5;
            int pageNumber = page ?? 1;

            switch (sortOrder.ToLower())
            {
                case "name":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.Name);
                    else
                        result = result.OrderBy(c => c.Name);
                    break;
                case "seats":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.Seats);
                    else
                        result = result.OrderBy(c => c.Seats);
                    break;
                case "largebagnum":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.LargeBagNum);
                    else
                        result = result.OrderBy(c => c.LargeBagNum);
                    break;
                case "smallbagnum":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.SmallBagNum);
                    else
                        result = result.OrderBy(c => c.SmallBagNum);
                    break;
                case "sizeid":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.SizeId);
                    else
                        result = result.OrderBy(c => c.SizeId);
                    break;
                case "rated":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.Rated);
                    else
                        result = result.OrderBy(c => c.Rated);
                    break;
                case "priceperday":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.PricePerDay);
                    else
                        result = result.OrderBy(c => c.PricePerDay);
                    break;
                case "airconditioning":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.AirConditioning);
                    else
                        result = result.OrderBy(c => c.AirConditioning);
                    break;
                case "automatictransmission":
                    if (sortDir.ToLower() == "desc")
                        result = result.OrderByDescending(c => c.AutomaticTransmission);
                    else
                        result = result.OrderBy(c => c.AutomaticTransmission);
                    break;
                default:
                    result = result.OrderBy(c => c.PricePerDay);
                    break;
            }

            var data = result.ToPagedList(pageNumber, pageSize);
            if (User.IsInRole(RoleName.CanManage))
                return this.PartialView("_CarView", data);
            else
                return this.PartialView("_CarViewReadOnly", data);
        }

        // GET: Cars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }
        [Authorize(Roles = RoleName.CanManage)]
        // GET: Cars/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cars/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Create([Bind(Include = "CarId,Name,Seats,LargeBagNum,SmallBagNum,SizeId,Rated,Photo,PhotoDB,PricePerDay,AirConditioning,AutomaticTransmission,Available")] CarViewModel carVm)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<CarViewModel, Car>().ForMember(c => c.Photo, opt => opt.Ignore());
                var car = Mapper.Map<Car>(carVm);
                if (carVm.Photo != null)
                {
                    car.Photo = ImageConverter.ByteArrayFromPostedFile(carVm.Photo);
                }
                db.Cars.Add(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(carVm);
        }

        // GET: Cars/Edit/5
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            Mapper.CreateMap<Car, CarViewModel>().ForMember(c => c.Photo, opt => opt.Ignore());
            CarViewModel cVm = Mapper.Map<CarViewModel>(car);
            cVm.PhotoDB = car.Photo;
            return View(cVm);
        }

        // POST: Cars/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Edit([Bind(Include = "CarId,Name,Seats,LargeBagNum,SmallBagNum,SizeId,Rated,Photo,PhotoDB,PricePerDay,AirConditioning,AutomaticTransmission,Available")] CarViewModel carVm)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<CarViewModel, Car>().ForMember(c => c.Photo, opt => opt.Ignore());
                var car = Mapper.Map<Car>(carVm);
                car.Photo = carVm.Photo != null ? ImageConverter.ByteArrayFromPostedFile(carVm.Photo) : carVm.PhotoDB;
                db.Cars.AddOrUpdate(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(carVm);
        }

        // GET: Cars/Delete/5
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult DeleteConfirmed(int id)
        {
            Car car = db.Cars.Find(id);
            db.Cars.Remove(car);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
