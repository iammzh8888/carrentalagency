﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarRentalAgency.Models;
using Microsoft.AspNet.Identity;
using PagedList;

namespace CarRentalAgency.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Orders
        [Authorize]
        public ActionResult Index(string sortDir, string searchInput, string currentFilter, int? page, string sortOrder = "")
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var orders = db.Orders.AsQueryable();
            if (!User.IsInRole(RoleName.CanManage))
            {
                string userId = User.Identity.GetUserId().ToLower();
                orders = orders.Where(o => o.UserId.ToLower().Contains(userId));
            }
            orders = orders.OrderBy(o => o.Id);

            int pageSize = 5;
            int pageNumber = page ?? 1;
            string para = Request.QueryString["page"];
            if (string.IsNullOrEmpty(para) && string.IsNullOrEmpty(searchInput) && string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(sortDir))
            {
                var data1 = orders.ToPagedList(pageNumber, pageSize);

                return this.View(data1);
            }

            if (!string.IsNullOrEmpty(searchInput))
                orders = orders.Where(o => (o.Location.ToLower().Contains(searchInput.ToLower()) || o.Destination.ToLower().Contains(searchInput.ToLower()) || o.UserName.ToLower().Contains(searchInput.ToLower())));

            switch (sortOrder.ToLower())
            {
                case "id":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.Id);
                    else
                        orders = orders.OrderBy(o => o.Id);
                    break;
                case "location":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.Location);
                    else
                        orders = orders.OrderBy(o => o.Location);
                    break;
                case "destination":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.Destination);
                    else
                        orders = orders.OrderBy(o => o.Destination);
                    break;
                case "startime":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.StarTime);
                    else
                        orders = orders.OrderBy(o => o.StarTime);
                    break;
                case "endtime":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.EndTime);
                    else
                        orders = orders.OrderBy(o => o.EndTime);
                    break;
                case "dealtime":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.DealTime);
                    else
                        orders = orders.OrderBy(o => o.DealTime);
                    break;
                case "totalprice":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.TotalPrice);
                    else
                        orders = orders.OrderBy(o => o.TotalPrice);
                    break;
                case "username":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.UserName);
                    else
                        orders = orders.OrderBy(o => o.UserName);
                    break;
                case "carid":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.CarId);
                    else
                        orders = orders.OrderBy(o => o.CarId);
                    break;
                default:
                    orders = orders.OrderBy(o => o.Id);
                    break;
            }

            var result = orders.ToPagedList(pageNumber, pageSize);
            return PartialView("_OrderView", result);
        }

        [Authorize]
        public ActionResult Search(string sortDir, string searchInput, string currentFilter, int? page, OrderViewModel orderVm, string sortOrder = "")
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var orders = db.Orders.AsQueryable();
            if (!User.IsInRole(RoleName.CanManage))
            {
                string userId = User.Identity.GetUserId().ToLower();
                orders = orders.Where(o => o.UserId.ToLower().Contains(userId));
            }
            orders = orders.OrderBy(o => o.Id);

            int pageSize = 5;
            int pageNumber = page ?? 1;

            if (!string.IsNullOrEmpty(searchInput))
                orders = orders.Where(o => (o.Location.ToLower().Contains(searchInput.ToLower()) || o.Destination.ToLower().Contains(searchInput.ToLower()) || o.UserName.ToLower().Contains(searchInput.ToLower())));

            switch (sortOrder.ToLower())
            {
                case "id":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.Id);
                    else
                        orders = orders.OrderBy(o => o.Id);
                    break;
                case "location":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.Location);
                    else
                        orders = orders.OrderBy(o => o.Location);
                    break;
                case "destination":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.Destination);
                    else
                        orders = orders.OrderBy(o => o.Destination);
                    break;
                case "startime":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.StarTime);
                    else
                        orders = orders.OrderBy(o => o.StarTime);
                    break;
                case "endtime":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.EndTime);
                    else
                        orders = orders.OrderBy(o => o.EndTime);
                    break;
                case "dealtime":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.DealTime);
                    else
                        orders = orders.OrderBy(o => o.DealTime);
                    break;
                case "totalprice":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.TotalPrice);
                    else
                        orders = orders.OrderBy(o => o.TotalPrice);
                    break;
                case "username":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.UserName);
                    else
                        orders = orders.OrderBy(o => o.UserName);
                    break;
                case "carid":
                    if (sortDir.ToLower() == "desc")
                        orders = orders.OrderByDescending(o => o.CarId);
                    else
                        orders = orders.OrderBy(o => o.CarId);
                    break;
                default:
                    orders = orders.OrderBy(o => o.Id);
                    break;
            }

            var result = orders.ToPagedList(pageNumber, pageSize);
            return PartialView("_OrderView", result);
        }

        // GET: Orders/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            order.Car = db.Cars.Find(order.CarId);
            if (order?.Car == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Confirmation/5
        [Authorize]
        public ActionResult Confirmation()
        {
            return View();
        }

        // GET: Orders/Create
        [Authorize]
        public ActionResult Create(int? carId)
        {
            if (carId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(carId);
            if (car == null)
            {
                return HttpNotFound();
            }

            OrderViewModel orderVm = new OrderViewModel();
            orderVm.Car = car;
            orderVm.StarTime = DateTime.Now.AddDays(1);
            orderVm.EndTime = DateTime.Now.AddDays(3);
            return View(orderVm);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Location,Destination,StarTime,EndTime,DealTime,CarId,UserId,UserName")] OrderViewModel orderVm)
        {
            if (ModelState.IsValid)
            {

                orderVm.Car = db.Cars.Find(orderVm.CarId);
                if (string.IsNullOrEmpty(orderVm.Destination))
                {
                    orderVm.Destination = orderVm.Location;
                }
                orderVm.TotalPrice = orderVm.Car.PricePerDay * (orderVm.EndTime - orderVm.StarTime).Days;
                orderVm.UserId = User.Identity.GetUserId();
                orderVm.UserName = User.Identity.GetUserName();
                orderVm.DealTime = DateTime.Now;
                Mapper.CreateMap<OrderViewModel, Order>();
                Order order = Mapper.Map<Order>(orderVm);
                orderVm.Car.Available = false;
                db.Cars.AddOrUpdate(orderVm.Car);
                db.Orders.Add(order);
                db.SaveChanges();

                return RedirectToAction("Details", new {id = order.Id });
            }

            return View(orderVm);
        }

        // GET: Orders/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarId = new SelectList(db.Cars, "CarId", "Name", order.CarId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,Location,Destination,StarTime,EndTime,DealTime,TotalPrice,CarId,UserId,UserName")] Order order)
        {
            if (ModelState.IsValid)
            {
                Order tempOrder = db.Orders.Find(order.Id);
                if (tempOrder == null)
                {
                    return HttpNotFound();
                }
                if (tempOrder.CarId != order.CarId)
                {
                    order.Car = db.Cars.Find(order.CarId);
                    if (order.Car == null)
                    {
                        return HttpNotFound();
                    }
                    
                    Car car1 = db.Cars.Find(tempOrder.CarId);
                    if (car1 == null)
                    {
                        return HttpNotFound();
                    }
                    order.Car.Available = false;
                    db.Cars.AddOrUpdate(order.Car);
                    car1.Available = true;
                    db.Cars.AddOrUpdate(car1);
                }
                order.TotalPrice = order.Car.PricePerDay * (order.EndTime - order.StarTime).Days;

                db.Orders.AddOrUpdate(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
